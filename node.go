package layout

import (
	"gitlab.com/bytewright/eagle/input"
	"gitlab.com/bytewright/gmath/mathi"
)

type In interface {
	input.In
	Clipped() bool
}

type Node interface {
	// MinsSize calculates the minimum size for each component
	MinSize() mathi.Vec2
	// Layout does the layouting of the components
	Layout(position, size mathi.Vec2)
	// Input handles input events, mouse and keyboard
	Input(in In) bool
}

func FromInput(in input.In) In {
	return &unclippedInput{In: in}
}

type unclippedInput struct {
	input.In
}

func (ui *unclippedInput) Clipped() bool {
	return false
}

func clipp(i In, area mathi.Box2) In {
	switch ti := i.(type) {
	case *unclippedInput:
		return &clippedInput{
			In:   ti.In,
			area: area,
		}
	case *clippedInput:
		return &clippedInput{
			In:   ti.In,
			area: area,
		}
	default:
		panic("invalid input type")
	}
}

type clippedInput struct {
	input.In
	area mathi.Box2
}

func (ci *clippedInput) Clipped() bool {
	return !ci.area.Contains(mathi.Vec2FromFloat32(ci.In.Mouse()))
}
