module gitlab.com/bytewright/eagle/layout

go 1.19

require (
	gitlab.com/akabio/expect v0.9.9
	gitlab.com/bytewright/eagle/input v0.0.2
	gitlab.com/bytewright/gmath v0.19.5
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/ghodss/yaml v1.0.0 // indirect
	github.com/go-gl/glfw/v3.3/glfw v0.0.0-20220806181222-55e207c401ad // indirect
	github.com/sergi/go-diff v1.2.0 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
