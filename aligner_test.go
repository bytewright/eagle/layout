package layout

import (
	"testing"

	"gitlab.com/akabio/expect"
)

func TestAlignerMin(t *testing.T) {
	a := &aligner{
		Sizes: []int{10, 20, 30},
		Min:   60,
	}

	expect.Value(t, "min", a.min()).ToBe(60)
	expect.Value(t, "segments", a.segments()).ToBe([][2]int{
		{0, 10},
		{10, 20},
		{30, 30},
	})
}

func TestNonMinEven(t *testing.T) {
	a := &aligner{
		Sizes: []int{10, 20, 30},
		Min:   100,
	}

	expect.Value(t, "min", a.min()).ToBe(60)
	expect.Value(t, "segments", a.segments()).ToBe([][2]int{
		{0, 23},
		{23, 33},
		{56, 44},
	})
}

func TestMinPadNoStretch(t *testing.T) {
	a := &aligner{
		Sizes:   []int{10, 20, 30},
		Min:     64,
		Padding: 2,
	}

	expect.Value(t, "min", a.min()).ToBe(64)
	expect.Value(t, "segments", a.segments()).ToBe([][2]int{
		{0, 10},
		{12, 20},
		{34, 30},
	})
}

func TestNonMinNoStretchPad(t *testing.T) {
	a := &aligner{
		Sizes:   []int{10, 20, 30},
		Min:     100,
		Padding: 2,
	}

	expect.Value(t, "min", a.min()).ToBe(64)
	expect.Value(t, "segments", a.segments()).ToBe([][2]int{
		{0, 22},
		{24, 32},
		{58, 42},
	})
}

func TestStretchFirstPad(t *testing.T) {
	a := &aligner{
		Sizes:   []int{10, 20, 30},
		Stretch: []float32{1},
		Min:     100,
		Padding: 2,
	}

	expect.Value(t, "min", a.min()).ToBe(64)
	expect.Value(t, "segments", a.segments()).ToBe([][2]int{
		{0, 46},
		{48, 20},
		{70, 30},
	})
}

func TestStretchMixedPad(t *testing.T) {
	a := &aligner{
		Sizes:   []int{10, 20, 30},
		Stretch: []float32{1, 0, 0.5},
		Min:     100,
		Padding: 2,
	}

	expect.Value(t, "min", a.min()).ToBe(64)
	expect.Value(t, "segments", a.segments()).ToBe([][2]int{
		{0, 34},
		{36, 20},
		{58, 42},
	})
}
