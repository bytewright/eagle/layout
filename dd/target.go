package dd

type target[I any] struct {
	slot      Slot[I]
	onRemoved func(I)
	onAdded   func(I)
}
