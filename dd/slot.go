package dd

import (
	"gitlab.com/bytewright/eagle/layout"
	"gitlab.com/bytewright/gmath/mathi"
)

type SlotNode[I any] struct {
	layout.Node

	onSelect  func()
	onRemoved func(item I)
	onAdded   func(item I)
}

func MakeSlot[I any](state *DragState[I], slot Slot[I], slotState *SlotState, w, h int) *SlotNode[I] {
	slotArea := layout.AreaI(w, h).
		OnChanged(func(pos, size mathi.Vec2) {
			slotState.Box.From = pos
			slotState.Box.To = pos.Add(size)
		})

	downed := false

	slotNode := &SlotNode[I]{}
	slotNode.Node = layout.Callback(layout.Event(slotArea).
		OnOver(func(c *layout.Context) {
			slotState.Hoover = true
			if state.Active() {
				if canDrop(state, slot) {
					state.target = &target[I]{
						slot:      slot,
						onRemoved: slotNode.handleRemoved,
						onAdded:   slotNode.handleAdded,
					}
				}
			}
			state.hoover = slot.Get()
		}).
		OnOut(func(c *layout.Context) {
			slotState.Hoover = false
			if state.Active() {
				state.target = nil
			}
			var empty I
			state.hoover = empty
		}).
		OnDown(func(m int, c *layout.Context) {
			if m == 1 {
				if state.Active() || state.candidate() {
					return
				}
				if !state.empty(slot.Get()) {
					state.init(newDraggable(
						slot,
						c.Pos,
						slotNode.handleRemoved,
						slotNode.handleAdded,
					), c.ScreenPos, func() {
						// drag has started so mouse down state is no more (no onSelect event)
						downed = false
					})
				}
			}
			// means mouse is down but no drag has started yet
			downed = true
		}).
		OnUp(func(m int, c *layout.Context) {
			if downed {
				if slotNode.onSelect != nil {
					slotNode.onSelect()
				}
			}
		}), func() {
		slotState.Target = canDrop(state, slot)
	})

	return slotNode
}

func (s *SlotNode[I]) OnSelect(onSelect func()) *SlotNode[I] {
	s.onSelect = onSelect
	return s
}

func (s *SlotNode[I]) OnRemoved(onRemoved func(item I)) *SlotNode[I] {
	s.onRemoved = onRemoved
	return s
}

func (s *SlotNode[I]) handleRemoved(i I) {
	if s.onRemoved != nil {
		s.onRemoved(i)
	}
}

func (s *SlotNode[I]) OnAdded(onAdded func(item I)) *SlotNode[I] {
	s.onAdded = onAdded
	return s
}

func (s *SlotNode[I]) handleAdded(i I) {
	if s.onAdded != nil {
		s.onAdded(i)
	}
}

func canDrop[I any](state *DragState[I], target Slot[I]) bool {
	if !state.Active() {
		return false
	}

	if !target.Accept(state.draggable.item) {
		return false
	}

	return state.draggable.from.Accept(target.Get())
}
