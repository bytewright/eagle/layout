package dd

import "gitlab.com/bytewright/eagle/input"

func (d *DragState[I]) HandleEvents(in input.In) {
	d.mousePos = in.Mouse()

	if d.candidate() {
		if d.Position().Sub(in.Mouse()).Len() > 3 {
			d.activate()
		}

		if in.JustReleased(input.MouseButtonLeft) {
			d.reset()
		}
	}

	if d.Active() {
		if in.JustReleased(input.MouseButtonLeft) {
			if !d.handleDrop() {
				d.revert()
			}
			d.reset()
		}
	}
}

func (d *DragState[I]) handleDrop() bool {
	item := d.Item()
	if !d.empty(item) {
		if d.target != nil {
			ng := d.target.slot.Get()
			d.target.slot.Set(item, d.draggable.from)
			d.draggable.onRemoved(item)
			if !d.empty(ng) {
				d.draggable.from.Set(ng, d.target.slot)
				d.target.onRemoved(ng)
				d.draggable.onAdded(ng)
			}
			d.target.onAdded(item)
			return true
		}
	}
	return false
}
