package dd

import "gitlab.com/bytewright/gmath/mathi"

type SlotState struct {
	Box    mathi.Box2
	Hoover bool
	Target bool
}
