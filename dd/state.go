package dd

import (
	"gitlab.com/bytewright/gmath/math32"
)

type Slot[I any] interface {
	// set a new item into slot
	Set(item I, from Slot[I])
	// get current item
	Get() I
	// will accept given item
	Accept(item I) bool
}

type DragState[I any] struct {
	// if a drag process has bin initialized this will be set
	draggable *draggable[I]
	// if isCandidate is set draggable is set but mouse has not moved enough yet to start dragging
	isCandidate bool
	// position of when drag process started
	position math32.Vec2

	// is triggered when a drag really starts (isCandidate = false)
	onActivate func()

	// if currently hoovering over a valid target this will be set
	target *target[I]

	// helper func to check if an item is empty
	empty func(I) bool

	// currently hoovered item
	hoover I

	mousePos math32.Vec2
}

func New[I any](empty func(I) bool) *DragState[I] {
	return &DragState[I]{
		empty: empty,
	}
}

func (d *DragState[I]) init(drg *draggable[I], pos math32.Vec2, onActivate func()) {
	d.draggable = drg
	d.isCandidate = true
	d.position = pos
	d.onActivate = onActivate
}

func (d *DragState[I]) activate() {
	d.isCandidate = false
	var i I
	d.draggable.from.Set(i, nil)
	d.onActivate()
	d.target = nil
}

func (d *DragState[I]) reset() {
	d.draggable = nil
}

func (d *DragState[I]) Active() bool {
	return d.draggable != nil && !d.isCandidate
}

func (d *DragState[I]) Hoover() I {
	return d.hoover
}

func (d *DragState[I]) candidate() bool {
	return d.draggable != nil && d.isCandidate
}

func (d *DragState[I]) Position() math32.Vec2 {
	return d.position
}

// Draggable delegate
func (d *DragState[I]) Offset() math32.Vec2 {
	if d.Active() {
		return d.draggable.Offset()
	}
	return math32.Vec2{}
}

func (d *DragState[I]) Mouse() math32.Vec2 {
	return d.mousePos
}

func (d *DragState[I]) Item() I {
	if d.Active() {
		return d.draggable.item
	}
	var i I
	return i
}

func (d *DragState[I]) revert() {
	if d.Active() {
		d.draggable.revert()
	}
}
