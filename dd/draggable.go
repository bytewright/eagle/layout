package dd

import "gitlab.com/bytewright/gmath/math32"

type draggable[I any] struct {
	item      I
	from      Slot[I]
	offset    math32.Vec2
	onRemoved func(I)
	onAdded   func(I)
}

func newDraggable[I any](slot Slot[I], offset math32.Vec2, onRemoved, onAdded func(I)) *draggable[I] {
	return &draggable[I]{
		item:      slot.Get(),
		from:      slot,
		offset:    offset,
		onRemoved: onRemoved,
		onAdded:   onAdded,
	}
}

// sets the draged item back to original slot
func (gd *draggable[I]) revert() {
	gd.from.Set(gd.item, gd.from)
}

func (gd *draggable[I]) Offset() math32.Vec2 {
	return gd.offset
}
