package layout

import (
	"gitlab.com/bytewright/gmath/mathi"
)

type SwitchNode struct {
	Child func() Node
}

// SwitchOn allows creating a new subtree for every different
// provided key. Will only recall create func when the given key
// has not ha a create call yet.
func SwitchOn[T comparable](key func() T, create func() Node) *SwitchNode {
	cache := map[T]Node{}

	return &SwitchNode{
		Child: func() Node {
			k := key()
			node, has := cache[k]
			if !has {
				node = create()
				cache[k] = node
			}

			return node
		},
	}
}

// Switch between different layout subtrees
func Switch(f func() Node) *SwitchNode {
	return &SwitchNode{
		Child: f,
	}
}

func (a *SwitchNode) MinSize() mathi.Vec2 {
	return a.Child().MinSize()
}

func (a *SwitchNode) Layout(pos, size mathi.Vec2) {
	a.Child().Layout(pos, size)
}

func (a *SwitchNode) Input(in In) bool {
	return a.Child().Input(in)
}
