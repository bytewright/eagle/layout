package wctrl

import (
	"gitlab.com/bytewright/eagle/layout"
	"gitlab.com/bytewright/gmath/math32"
	"gitlab.com/bytewright/gmath/mathi"
)

func UpdateArea(a *math32.Box2) func(p, s mathi.Vec2) {
	return func(p, s mathi.Vec2) {
		a.From = p.Float32()
		a.To = p.Add(s).Float32()
	}
}

func UpdateAreaNode(a *math32.Box2, size mathi.Vec2) layout.Node {
	return layout.Area(size).OnChanged(UpdateArea(a))
}

func UpdateAreaNodeI(a *math32.Box2, w, h int) layout.Node {
	return layout.AreaI(w, h).OnChanged(UpdateArea(a))
}

func UpdateAreaFor(a *math32.Box2, n layout.Node) layout.Node {
	return layout.EqualColumns(n).OnChanged(UpdateArea(a))
}
