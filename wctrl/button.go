package wctrl

import (
	"gitlab.com/bytewright/eagle/layout"
	"gitlab.com/bytewright/eagle/layout/wstate"
	"gitlab.com/bytewright/gmath/mathi"
)

func MakeButton(state *wstate.Button, size mathi.Vec2, call func()) layout.Node {
	return layout.Event(UpdateAreaNode(&state.Area, size)).
		OnOver(func(*layout.Context) {
			state.Hover = true
		}).
		OnOut(func(*layout.Context) {
			state.Hover = false
		}).
		OnDown(func(b int, _ *layout.Context) {
			if b == 1 {
				state.Pressed = true
			}
		}).
		OnUp(func(b int, _ *layout.Context) {
			if b == 1 {
				state.Pressed = false
			}
		}).
		OnClick(func(b int, _ *layout.Context) {
			if b == 1 {
				call()
			}
		})
}
