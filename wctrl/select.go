package wctrl

import (
	"gitlab.com/bytewright/eagle/layout"
	"gitlab.com/bytewright/eagle/layout/wstate"
	"gitlab.com/bytewright/gmath/mathi"
)

func MakeSelect[O comparable](st *wstate.Select[O], size mathi.Vec2, cb func(o O), options ...O) []layout.Node {
	if len(st.Options) != 0 {
		panic("select state must not have options")
	}

	nodes := []layout.Node{}

	for range options {
		st.Options = append(st.Options, wstate.SelectOption{})
	}

	for i := range options {
		i := i

		evt := layout.Event(UpdateAreaNode(&st.Options[i].Area, size)).
			OnClick(func(b int, ctx *layout.Context) {
				if b != 1 {
					return
				}
				if st.Active == options[i] {
					return
				}
				if st.Options[i].Disabled {
					return
				}

				for oi := range st.Options {
					st.Options[oi].Active = false
				}
				st.Options[i].Active = true
				st.Active = options[i]

				cb(options[i])
			}).
			OnOver(func(*layout.Context) {
				st.Options[i].Hover = true
			}).
			OnOut(func(*layout.Context) {
				st.Options[i].Hover = false
			}).
			OnDown(func(b int, _ *layout.Context) {
				if b == 1 {
					st.Options[i].Pressed = true
				}
			}).
			OnUp(func(b int, _ *layout.Context) {
				if b == 1 {
					st.Options[i].Pressed = false
				}
			})

		nodes = append(nodes, evt)
	}

	if len(options) > 0 {
		st.Active = options[0]
		st.Options[0].Active = true
	}

	return nodes
}
