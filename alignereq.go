package layout

import (
	"gitlab.com/bytewright/gmath/mathi"
)

type alignerEq struct {
	Sizes   []int
	Min     int
	Padding int
}

func (a *alignerEq) add(size int) {
	a.Sizes = append(a.Sizes, size)
}

func (a *alignerEq) maxElementWidth() int {
	max := 0
	for _, v := range a.Sizes {
		max = mathi.Max(v, max)
	}
	return max
}

func (a *alignerEq) min() int {
	return (a.maxElementWidth()+a.Padding)*len(a.Sizes) - a.Padding
}

func (a *alignerEq) segments() [][2]int {
	segs := [][2]int{}
	if len(a.Sizes) == 0 {
		return segs
	}

	min := a.min()

	remaining := a.Min - min
	partsLeft := len(a.Sizes)

	extra := []int{}
	for partsLeft > 0 {
		next := remaining / partsLeft
		extra = append(extra, next)
		remaining -= next
		partsLeft--
	}

	maxElement := a.maxElementWidth()

	from := 0
	for i := 0; i < len(a.Sizes); i++ {
		next := maxElement + extra[i]
		segs = append(segs, [2]int{from, next})
		from += next + a.Padding
	}
	return segs
}
