package layout

import (
	"gitlab.com/bytewright/gmath/mathi"
)

type AreaNode struct {
	Size    mathi.Vec2
	pos     mathi.Vec2
	size    mathi.Vec2
	Changed func(pos mathi.Vec2, size mathi.Vec2)
}

func Area(s mathi.Vec2) *AreaNode {
	return &AreaNode{
		Size: s,
	}
}

func AreaI(w, h int) *AreaNode {
	return &AreaNode{
		Size: mathi.Vec2{w, h},
	}
}

func (a *AreaNode) OnChanged(changed func(pos mathi.Vec2, size mathi.Vec2)) *AreaNode {
	a.Changed = changed
	return a
}

func (a *AreaNode) Update() error {
	return nil
}

func (a *AreaNode) MinSize() mathi.Vec2 {
	return a.Size
}

func (a *AreaNode) Layout(pos, size mathi.Vec2) {
	changed := a.pos != pos || a.size != size
	a.pos = pos
	a.size = size
	if changed && a.Changed != nil {
		a.Changed(pos, size)
	}
}

func (r *AreaNode) Input(In) bool {
	return false
}
