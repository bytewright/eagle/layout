package layout

// aligner does the layouting in one direction
type aligner struct {
	Sizes   []int
	Stretch []float32
	Min     int
	Padding int
}

func (a *aligner) add(v ...int) {
	a.Sizes = append(a.Sizes, v...)
}

func (a *aligner) min() int {
	tot := 0
	for _, v := range a.Sizes {
		tot += v + a.Padding
	}

	return tot - a.Padding
}

func (a *aligner) segments() [][2]int {
	segs := [][2]int{}
	if len(a.Sizes) == 0 {
		return segs
	}

	for len(a.Stretch) < len(a.Sizes) {
		a.Stretch = append(a.Stretch, 0)
	}

	allZero := true
	for _, v := range a.Stretch {
		if v != 0 {
			allZero = false
		}
	}

	if allZero {
		a.Stretch = a.Stretch[:0]
		for len(a.Stretch) < len(a.Sizes) {
			a.Stretch = append(a.Stretch, 1)
		}
	}

	remaining := a.Min - a.min()
	partsLeft := len(a.Sizes)

	totalStretch := float32(0)
	for _, st := range a.Stretch {
		totalStretch += st
	}

	extra := []int{}
	for _, str := range a.Stretch {
		if totalStretch < 0.000001 {
			extra = append(extra, 0)
			continue
		}
		next := int(float32(remaining) * str / totalStretch)
		extra = append(extra, next)
		remaining -= next
		partsLeft--
		totalStretch -= str
	}

	from := 0
	for i := 0; i < len(a.Sizes); i++ {
		next := a.Sizes[i] + extra[i]
		segs = append(segs, [2]int{from, next})
		from += next + a.Padding
	}
	return segs
}
