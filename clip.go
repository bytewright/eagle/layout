package layout

import (
	"gitlab.com/bytewright/gmath/mathi"
)

type ClipNode struct {
	Child    Node
	Size     *mathi.Vec2
	lastPos  mathi.Vec2
	lastSize mathi.Vec2
}

func Clip(n Node) *ClipNode {
	return &ClipNode{
		Child: n,
	}
}

func (p *ClipNode) ToSize(size mathi.Vec2) *ClipNode {
	p.Size = &size
	return p
}

func (p *ClipNode) MinSize() mathi.Vec2 {
	if p.Size != nil {
		return *p.Size
	}
	return p.Child.MinSize()
}

func (p *ClipNode) Layout(pos, size mathi.Vec2) {
	p.lastPos = pos
	p.lastSize = size
	p.Child.Layout(pos, size)
}

func (p *ClipNode) Input(in In) bool {
	if p.Size != nil {
		return p.Child.Input(clipp(in, mathi.Box2{From: p.lastPos, To: p.lastPos.Add(*p.Size)}))
	}

	return p.Child.Input(clipp(in, mathi.Box2{From: p.lastPos, To: p.lastPos.Add(p.lastSize)}))
}
