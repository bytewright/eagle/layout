package layout

import (
	"testing"

	"gitlab.com/akabio/expect"
)

func TestEqAlignMin(t *testing.T) {
	a := &alignerEq{
		Sizes: []int{10, 20, 30},
		Min:   90,
	}

	expect.Value(t, "min", a.min()).ToBe(90)
	expect.Value(t, "segments", a.segments()).ToBe([][2]int{
		{0, 30},
		{30, 30},
		{60, 30},
	})
}

func TestEqAlignNonMin(t *testing.T) {
	a := &alignerEq{
		Sizes: []int{10, 20, 30},
		Min:   100,
	}

	expect.Value(t, "min", a.min()).ToBe(90)
	expect.Value(t, "segments", a.segments()).ToBe([][2]int{
		{0, 33},
		{33, 33},
		{66, 34},
	})
}

func TestEqAlignMinPad(t *testing.T) {
	a := &alignerEq{
		Sizes:   []int{10, 20, 30},
		Min:     94,
		Padding: 2,
	}

	expect.Value(t, "min", a.min()).ToBe(94)
	expect.Value(t, "segments", a.segments()).ToBe([][2]int{
		{0, 30},
		{32, 30},
		{64, 30},
	})
}

func TestEqAlignNonMinPad(t *testing.T) {
	a := &alignerEq{
		Sizes:   []int{10, 20, 30},
		Min:     100,
		Padding: 2,
	}

	expect.Value(t, "min", a.min()).ToBe(94)
	expect.Value(t, "segments", a.segments()).ToBe([][2]int{
		{0, 32},
		{34, 32},
		{68, 32},
	})
}
