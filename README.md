Layout
======

Layouting for simple user interfaces.

Does the positioning and input handling but no rendering.

layout package
--------------

Contains elements to build a composite tree of the layout like:

    Rows
      Columns
        Area(20, 10)
        Area(20, 10)
        Padding(3)
          Area(40, 10)
      Area(100, 300)
    Area(50, 50)

It calculates the minimum size of each node and calculates position
and size for each node given a desired screen size.

When adding Event nodes it will add events like mouseOver/Out and
click/release events.

