package layout

import (
	"gitlab.com/bytewright/gmath/mathi"
)

type RowsNode struct {
	Children []Node
	Stretch  []float32
	Changed  func(position, size mathi.Vec2)

	pos  mathi.Vec2
	size mathi.Vec2
}

func Rows(n ...Node) *RowsNode {
	return &RowsNode{
		Children: n,
		Stretch:  make([]float32, len(n)),
	}
}

func (r *RowsNode) Append(n Node, stretch float32) *RowsNode {
	r.Children = append(r.Children, n)
	r.Stretch = append(r.Stretch, stretch)
	return r
}

func (r *RowsNode) OnChanged(changed func(position, size mathi.Vec2)) *RowsNode {
	r.Changed = changed
	return r
}

func (r *RowsNode) MinSize() mathi.Vec2 {
	maxWidth := 0

	aligner := &aligner{}
	for _, child := range r.Children {
		ms := child.MinSize()
		aligner.add(ms[1])
		if ms[0] > maxWidth {
			maxWidth = ms[0]
		}
	}

	return mathi.Vec2{maxWidth, aligner.min()}
}

func (r *RowsNode) Layout(pos, size mathi.Vec2) {
	aligner := &aligner{Min: size[1], Stretch: r.Stretch}

	maxWidth := 0
	for _, child := range r.Children {
		ms := child.MinSize()
		aligner.add(ms[1])
		if ms[0] > maxWidth {
			maxWidth = ms[0]
		}
	}

	segments := aligner.segments()

	for i, child := range r.Children {
		child.Layout(
			pos.Add(mathi.Vec2{0, segments[i][0]}),
			mathi.Vec2{
				size[0],
				segments[i][1],
			},
		)
	}

	if r.Changed != nil {
		if r.pos != pos && r.size != size {
			r.Changed(pos, size)
		}
	}
	r.size = size
	r.pos = pos
}

func (r *RowsNode) Input(in In) bool {
	h := false
	for _, cn := range r.Children {
		handled := cn.Input(in)
		if handled {
			h = true
		}
	}
	return h
}
