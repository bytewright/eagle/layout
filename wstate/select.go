package wstate

import "gitlab.com/bytewright/gmath/math32"

type Select[O comparable] struct {
	Options []SelectOption
	Active  O
}

type SelectOption struct {
	Hover    bool
	Pressed  bool
	Disabled bool
	Active   bool
	Area     math32.Box2
}

func (s *SelectOption) State() string {
	if s.Active {
		return "a"
	}
	if s.Disabled {
		return "d"
	}
	if s.Pressed {
		return "p"
	}
	if s.Hover {
		return "h"
	}
	return "n"
}
