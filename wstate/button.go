package wstate

import "gitlab.com/bytewright/gmath/math32"

type Button struct {
	Hover    bool
	Pressed  bool
	Disabled bool
	Area     math32.Box2
}

func (b *Button) State() string {
	if b.Disabled {
		return "d"
	}
	if b.Pressed {
		return "p"
	}
	if b.Hover {
		return "h"
	}
	return "n"
}
