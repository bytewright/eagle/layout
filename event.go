package layout

import (
	"gitlab.com/bytewright/eagle/input"
	"gitlab.com/bytewright/gmath/math32"
	"gitlab.com/bytewright/gmath/mathi"
)

type EventNode struct {
	Child Node
	Over  func(*Context)
	Out   func(*Context)
	Down  func(int, *Context)
	Up    func(int, *Context)
	Click func(int, *Context)

	tl mathi.Vec2
	br mathi.Vec2

	lastHover  bool
	lastMouse1 bool

	lastPos mathi.Vec2
}

type Context struct {
	ScreenPos math32.Vec2
	Pos       math32.Vec2
}

func Event(n Node) *EventNode {
	return &EventNode{
		Child: n,
	}
}

func (e *EventNode) OnClick(click func(int, *Context)) *EventNode {
	e.Click = click
	return e
}

func (e *EventNode) OnOver(over func(*Context)) *EventNode {
	e.Over = over
	return e
}

func (e *EventNode) OnOut(out func(*Context)) *EventNode {
	e.Out = out
	return e
}

func (e *EventNode) OnDown(down func(int, *Context)) *EventNode {
	e.Down = down
	return e
}

func (e *EventNode) OnUp(up func(int, *Context)) *EventNode {
	e.Up = up
	return e
}

func (e *EventNode) MinSize() mathi.Vec2 {
	return e.Child.MinSize()
}

func (e *EventNode) Layout(pos, size mathi.Vec2) {
	e.tl = pos
	e.br = pos.Add(size)
	e.Child.Layout(pos, size)
	e.lastPos = pos
}

func (e *EventNode) Input(in In) bool {
	ctx := &Context{
		ScreenPos: in.Mouse(),
		Pos:       in.Mouse().Sub(e.lastPos.Float32()),
	}

	p := mathi.Vec2FromFloat32(in.Mouse())

	hover := p[0] > e.tl[0] && p[0] < e.br[0] &&
		p[1] > e.tl[1] && p[1] < e.br[1] && !in.Clipped()

	childHover := e.Child.Input(in)

	if childHover {
		hover = false
	}

	if hover && !e.lastHover {
		e.over(ctx)
	}

	if !hover && e.lastHover {
		e.out(ctx)
	}

	if hover {
		if in.Pressed(input.MouseButtonLeft) && !e.lastMouse1 {
			e.down(1, ctx)
		}

		if !in.Pressed(input.MouseButtonLeft) && e.lastMouse1 {
			e.click(1, ctx)
			e.up(1, ctx)
		}
	}

	e.lastHover = hover
	if hover {
		e.lastMouse1 = in.Pressed(input.MouseButtonLeft)
	} else {
		if e.lastMouse1 {
			e.up(1, ctx)
		}
		e.lastMouse1 = false
	}

	return hover || childHover
}

func (e *EventNode) click(b int, c *Context) {
	if e.Click != nil {
		e.Click(b, c)
	}
}

func (e *EventNode) up(b int, c *Context) {
	if e.Up != nil {
		e.Up(b, c)
	}
}

func (e *EventNode) down(b int, c *Context) {
	if e.Down != nil {
		e.Down(b, c)
	}
}

func (e *EventNode) over(c *Context) {
	if e.Over != nil {
		e.Over(c)
	}
}

func (e *EventNode) out(c *Context) {
	if e.Out != nil {
		e.Out(c)
	}
}
