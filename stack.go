package layout

import (
	"gitlab.com/bytewright/gmath/mathi"
)

type StackNode struct {
	Children        []Node
	AllReceiveInput bool
}

func Stack(ns ...Node) *StackNode {
	return &StackNode{
		Children: ns,
	}
}

func (s *StackNode) MinSize() mathi.Vec2 {
	min := mathi.Vec2{}
	for _, ch := range s.Children {
		cm := ch.MinSize()
		min[0] = mathi.Max(min[0], cm[0])
		min[1] = mathi.Max(min[1], cm[1])
	}
	return min
}

func (s *StackNode) Layout(pos, size mathi.Vec2) {
	for _, child := range s.Children {
		child.Layout(pos, size)
	}
}

func (s *StackNode) Input(in In) bool {
	for ci := len(s.Children) - 1; ci > -1; ci-- {
		c := s.Children[ci]
		handled := c.Input(in)
		if handled && !s.AllReceiveInput {
			return true
		}
	}
	return false
}
