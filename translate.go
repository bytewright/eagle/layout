package layout

import (
	"gitlab.com/bytewright/gmath/mathi"
)

type TranslateNode struct {
	Child Node
	By    mathi.Vec2
}

func Translate(n Node, by mathi.Vec2) *TranslateNode {
	return &TranslateNode{
		Child: n,
		By:    by,
	}
}

func (p *TranslateNode) MinSize() mathi.Vec2 {
	return p.Child.MinSize()
}

func (p *TranslateNode) Layout(pos, size mathi.Vec2) {
	p.Child.Layout(pos.Add(p.By), size)
}

func (p *TranslateNode) Input(in In) bool {
	return p.Child.Input(in)
}
