package layout

import (
	"gitlab.com/bytewright/gmath/mathi"
)

type RowsEqNode struct {
	Children []Node
	Changed  func(position, size mathi.Vec2)
	Padding  int

	pos  mathi.Vec2
	size mathi.Vec2
}

func EqualRows(n ...Node) *RowsEqNode {
	return &RowsEqNode{
		Children: n,
	}
}

func (r *RowsEqNode) OnChanged(changed func(position, size mathi.Vec2)) *RowsEqNode {
	r.Changed = changed
	return r
}

func (r *RowsEqNode) Pad(padding int) *RowsEqNode {
	r.Padding = padding
	return r
}

func (r *RowsEqNode) Append(n ...Node) *RowsEqNode {
	r.Children = append(r.Children, n...)
	return r
}

func (r *RowsEqNode) MinSize() mathi.Vec2 {
	maxWidth := 0

	aligner := &alignerEq{Min: 0, Padding: r.Padding}
	for _, child := range r.Children {
		ms := child.MinSize()
		aligner.add(ms[1])
		if ms[0] > maxWidth {
			maxWidth = ms[0]
		}
	}

	return mathi.Vec2{maxWidth, aligner.min()}
}

func (r *RowsEqNode) Layout(pos, size mathi.Vec2) {
	aligner := &alignerEq{Min: size[1], Padding: r.Padding}

	maxWidth := 0
	for _, child := range r.Children {
		ms := child.MinSize()
		aligner.add(ms[1])
		if ms[0] > maxWidth {
			maxWidth = ms[0]
		}
	}

	segments := aligner.segments()

	for i, child := range r.Children {
		child.Layout(
			pos.Add(mathi.Vec2{0, segments[i][0]}),
			mathi.Vec2{
				size[0],
				segments[i][1],
			},
		)
	}

	if r.Changed != nil {
		if r.pos != pos && r.size != size {
			r.Changed(pos, size)
		}
	}
	r.size = size
	r.pos = pos
}

func (r *RowsEqNode) Input(in In) bool {
	h := false
	for _, cn := range r.Children {
		handled := cn.Input(in)
		if handled {
			h = true
		}
	}
	return h
}
