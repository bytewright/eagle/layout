package layout

import (
	"gitlab.com/bytewright/gmath/math32"
	"gitlab.com/bytewright/gmath/mathi"
)

type PositionNode struct {
	Child       Node
	Pos         math32.Vec2
	StretchNode math32.Vec2
}

func Position(n Node) *PositionNode {
	return &PositionNode{
		Child: n,
	}
}

func (p *PositionNode) At(pos math32.Vec2) *PositionNode {
	p.Pos = pos
	return p
}

func (p *PositionNode) AtF(x, y float32) *PositionNode {
	p.Pos = math32.Vec2{x, y}
	return p
}

func (p *PositionNode) Stretch(s math32.Vec2) *PositionNode {
	p.StretchNode = s
	return p
}

func (p *PositionNode) StretchF(x, y float32) *PositionNode {
	p.StretchNode = math32.Vec2{x, y}
	return p
}

func (p *PositionNode) MinSize() mathi.Vec2 {
	return p.Child.MinSize()
}

func mulWithF(a int, b float32) int {
	return int(float32(a) * b)
}

func (p *PositionNode) Layout(pos, size mathi.Vec2) {
	cSize := p.MinSize()
	delta := size.Sub(cSize)
	deltaStretch := mathi.Vec2{mulWithF(delta[0], p.StretchNode[0]), mulWithF(delta[1], p.StretchNode[1])}
	cSize = cSize.Add(deltaStretch)
	delta = delta.Sub(deltaStretch)
	cPos := mathi.Vec2{mulWithF(delta[0], p.Pos[0]), mulWithF(delta[1], p.Pos[1])}.Add(pos)
	p.Child.Layout(cPos, cSize)
}

func (p *PositionNode) Input(in In) bool {
	return p.Child.Input(in)
}
