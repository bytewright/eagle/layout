package layout

import (
	"gitlab.com/bytewright/gmath/mathi"
)

type AbsoluteNode struct {
	Position  mathi.Vec2
	Size      mathi.Vec2
	Child     Node
	childSize mathi.Vec2
}

func Absolute(pos mathi.Vec2, n Node) *AbsoluteNode {
	return &AbsoluteNode{
		Position: pos,
		Child:    n,
	}
}

func AbsoluteI(x, y int, n Node) *AbsoluteNode {
	return &AbsoluteNode{
		Position: mathi.Vec2{x, y},
		Child:    n,
	}
}

func (a *AbsoluteNode) WithSize(s mathi.Vec2) *AbsoluteNode {
	a.Size = s
	return a
}

func (a *AbsoluteNode) MinSize() mathi.Vec2 {
	a.childSize = a.Child.MinSize().Max(a.Size)

	return a.childSize.Add(a.Position)
}

func (a *AbsoluteNode) Layout(pos, size mathi.Vec2) {
	a.Child.Layout(pos.Add(a.Position), a.childSize)
}

func (a *AbsoluteNode) Input(in In) bool {
	return a.Child.Input(in)
}
