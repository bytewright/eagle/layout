package layout

import (
	"gitlab.com/bytewright/gmath/mathi"
)

type CallbackNode struct {
	Child    Node
	Callback func()
}

// Callback executes the given callback during the layouting step
// of the node tree.
func Callback(n Node, callback func()) *CallbackNode {
	return &CallbackNode{
		Child:    n,
		Callback: callback,
	}
}

func (c *CallbackNode) MinSize() mathi.Vec2 {
	return c.Child.MinSize()
}

func (c *CallbackNode) Layout(pos, size mathi.Vec2) {
	c.Callback()
	c.Child.Layout(pos, size)
}

func (c *CallbackNode) Input(in In) bool {
	return c.Child.Input(in)
}
