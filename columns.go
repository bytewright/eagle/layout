package layout

import (
	"gitlab.com/bytewright/gmath/mathi"
)

type ColumnsNode struct {
	Children []Node
	Stretch  []float32
	Changed  func(position, size mathi.Vec2)
	Padding  int

	pos  mathi.Vec2
	size mathi.Vec2
}

func Columns(n ...Node) *ColumnsNode {
	return &ColumnsNode{
		Children: n,
		Stretch:  make([]float32, len(n)),
	}
}

func (r *ColumnsNode) Append(n Node, stretch float32) *ColumnsNode {
	r.Children = append(r.Children, n)
	r.Stretch = append(r.Stretch, stretch)
	return r
}

func (r *ColumnsNode) OnChanged(changed func(position, size mathi.Vec2)) *ColumnsNode {
	r.Changed = changed
	return r
}

func (r *ColumnsNode) WithPadding(padding int) *ColumnsNode {
	r.Padding = padding
	return r
}

func (c *ColumnsNode) MinSize() mathi.Vec2 {
	maxHeight := 0

	aligner := &aligner{Padding: c.Padding}
	for _, child := range c.Children {
		ms := child.MinSize()
		aligner.add(ms[0])
		if ms[1] > maxHeight {
			maxHeight = ms[1]
		}
	}

	return mathi.Vec2{aligner.min(), maxHeight}
}

func (c *ColumnsNode) Layout(pos, size mathi.Vec2) {
	aligner := &aligner{Min: size[0], Stretch: c.Stretch, Padding: c.Padding}

	maxHeight := 0
	for _, child := range c.Children {
		ms := child.MinSize()
		aligner.add(ms[0])
		if ms[1] > maxHeight {
			maxHeight = ms[1]
		}
	}

	segments := aligner.segments()

	for i, child := range c.Children {
		child.Layout(
			pos.Add(mathi.Vec2{segments[i][0], 0}),
			mathi.Vec2{
				segments[i][1],
				size[1],
			},
		)
	}

	if c.Changed != nil {
		if c.pos != pos && c.size != size {
			c.Changed(pos, size)
		}
	}
	c.size = size
	c.pos = pos
}

func (c *ColumnsNode) Input(in In) bool {
	h := false
	for _, cn := range c.Children {
		handled := cn.Input(in)
		if handled {
			h = true
		}
	}
	return h
}
