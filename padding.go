package layout

import (
	"gitlab.com/bytewright/gmath/mathi"
)

type PaddingNode struct {
	Child Node
	Sides [4]int // top/right/bottom/left
}

// Pad creates padding around given Node
// x           -> top=x, right=x, bottom=x , left=x
// x, y        -> top=x, right=y, bottom=x , left=y
// x, y, z     -> top=x, right=y, bottom=z , left=y
// x, y, z, w  -> top=x, right=y, bottom=z , left=w
func Pad(child Node, sides ...int) *PaddingNode {
	pds := &PaddingNode{
		Child: child,
	}

	switch len(sides) {
	case 1:
		pds.Sides = [4]int{sides[0], sides[0], sides[0], sides[0]}
	case 2:
		pds.Sides = [4]int{sides[0], sides[1], sides[0], sides[1]}
	case 3:
		pds.Sides = [4]int{sides[0], sides[1], sides[2], sides[1]}
	case 4:
		pds.Sides = [4]int{sides[0], sides[1], sides[2], sides[3]}
	}
	return pds
}

func (p *PaddingNode) Input(in In) bool {
	return p.Child.Input(in)
}

func (p *PaddingNode) Layout(pos, size mathi.Vec2) {
	p.Child.Layout(
		pos.Add(mathi.Vec2{
			p.Sides[3],
			p.Sides[0],
		}),
		size.Sub(mathi.Vec2{
			p.Sides[3] + p.Sides[1],
			p.Sides[0] + p.Sides[2],
		}),
	)
}

func (p *PaddingNode) MinSize() mathi.Vec2 {
	cm := p.Child.MinSize()
	return mathi.Vec2{
		p.Sides[3] + cm[0] + p.Sides[1],
		p.Sides[0] + cm[1] + p.Sides[2],
	}
}
