package layout

import (
	"gitlab.com/bytewright/gmath/mathi"
)

type ColumnsEqNode struct {
	Children []Node
	Changed  func(position, size mathi.Vec2)
	Padding  int

	pos  mathi.Vec2
	size mathi.Vec2
}

func EqualColumns(n ...Node) *ColumnsEqNode {
	return &ColumnsEqNode{
		Children: n,
	}
}

func (r *ColumnsEqNode) OnChanged(changed func(position, size mathi.Vec2)) *ColumnsEqNode {
	r.Changed = changed
	return r
}

func (r *ColumnsEqNode) Append(n ...Node) *ColumnsEqNode {
	r.Children = append(r.Children, n...)
	return r
}

func (r *ColumnsEqNode) WithPadding(padding int) *ColumnsEqNode {
	r.Padding = padding
	return r
}

func (c *ColumnsEqNode) MinSize() mathi.Vec2 {
	maxHeight := 0

	aligner := &alignerEq{Padding: c.Padding}
	for _, child := range c.Children {
		ms := child.MinSize()
		aligner.add(ms[0])
		if ms[1] > maxHeight {
			maxHeight = ms[1]
		}
	}

	return mathi.Vec2{aligner.min(), maxHeight}
}

func (c *ColumnsEqNode) Layout(pos, size mathi.Vec2) {
	aligner := &alignerEq{Min: size[0], Padding: c.Padding}

	maxHeight := 0
	for _, child := range c.Children {
		ms := child.MinSize()
		aligner.add(ms[0])
		if ms[1] > maxHeight {
			maxHeight = ms[1]
		}
	}

	segments := aligner.segments()

	for i, child := range c.Children {
		child.Layout(
			pos.Add(mathi.Vec2{segments[i][0], 0}),
			mathi.Vec2{
				segments[i][1],
				size[1],
			},
		)
	}

	if c.Changed != nil {
		if c.pos != pos && c.size != size {
			c.Changed(pos, size)
		}
	}
	c.size = size
	c.pos = pos
}

func (c *ColumnsEqNode) Input(in In) bool {
	h := false
	for _, cn := range c.Children {
		handled := cn.Input(in)
		if handled {
			h = true
		}
	}
	return h
}
